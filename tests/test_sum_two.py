from src.main import sum_two

def test_sum_two():
    result = sum_two(3, 2)
    assert 5 == result
    