import random
import re
import uuid
from typing import AsyncGenerator

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.exc import DatabaseError
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

import src.models as models
from main import app, get_db

DATABASE_URL = "sqlite+aiosqlite:///./test.db"

engine = create_async_engine(
    DATABASE_URL,
    echo=True,
    poolclass=StaticPool,
    connect_args={"check_same_thread": False},
)
async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


@pytest.fixture(scope="session", autouse=True)
async def create_db():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all, checkfirst=True)
    yield
    await engine.dispose()


async def get_test_db() -> AsyncGenerator[AsyncSession, None]:
    _session = async_session()
    try:
        yield _session
    except DatabaseError:
        await _session.rollback()
    finally:
        await _session.close()


app.dependency_overrides[get_db] = get_test_db

client = TestClient(app)


@pytest.mark.api
async def test_root():
    """Проверяем корневой эндпоинт"""
    with client.get("/") as response:
        assert response.status_code == 200

        # В сообщении ожидаются ссылки на основные эндпоинты
        message = response.json()["message"]
        assert re.search(r"/docs", message) is not None
        assert re.search(r"/redoc", message) is not None


@pytest.mark.api
async def test_docs():
    """Проверка на существование документации /docs"""
    response = client.get("/docs")
    assert response


@pytest.mark.api
async def test_redoc():
    """Проверка на существование документации /redoc"""
    response = client.get("/redoc")
    assert response


def get_random_string():
    return str(uuid.uuid4())


@pytest.mark.asyncio
async def test_create_dish() -> dict:
    """Проверка записи блюда в базу данных"""
    # Создадим записи с уникальными значениями
    dish = {
        "dish_name": "Тестовый тестер {0}".format(get_random_string()),
        "cooking_time": round(random.random() * 100 + 0.01, 10),
        "description": "Подробно {0}".format(get_random_string()),
        "ingredients": "Тесты {0}".format(get_random_string()),
    }

    # Записать рецепт
    response = client.post("/cookbook/dishes", json=dish)
    assert response.status_code == 201

    # Проверить результат записи
    data = response.json()
    assert "id" in data
    assert isinstance(data["id"], int)

    assert data["id"] != 0
    assert data.get("dish_name") == dish["dish_name"]
    assert data.get("cooking_time") == dish["cooking_time"]
    assert data.get("number_of_views") == 0

    # Вернуть для последующих связанных тестов
    return data


@pytest.mark.asyncio
async def test_one_dish() -> dict:
    """Проверка чтения блюда из базы данных"""
    dish = await test_create_dish()
    response = client.get("/cookbook/dishes/{0}".format(dish["id"]))
    assert response.status_code == 200

    # Проверить полученный результат
    data = response.json()
    assert data.get("dish_name") == dish.get("dish_name")
    assert data.get("cooking_time") == dish.get("cooking_time")
    assert data.get("description") is not None
    assert data.get("ingredients") is not None
    assert data.get("number_of_views", 0) == dish.get("number_of_views", 0) + 1

    # Вернуть для последующих связанных тестов
    return data


@pytest.mark.asyncio
async def test_many_dishes() -> list[dict]:
    """Проверка чтения списка блюд из базы данных"""
    dish = await test_create_dish()
    response = client.get("/cookbook/dishes")
    assert response.status_code == 200

    # Проверить полученный результат
    data = response.json()
    assert isinstance(data, list)
    checking_dish = [item for item in data if item.get("id") == dish["id"]]
    assert len(checking_dish) == 1
    assert checking_dish[0].get("number_of_views", 0) == 0

    return data


if __name__ == "__main__":
    print("test app")
