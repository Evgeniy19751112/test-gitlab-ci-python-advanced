""" Модуль базы данных кулинарной книги """

import typing

from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine
from sqlalchemy.orm import Session, declarative_base, sessionmaker

# Настройка подключения к БД
DATABASE_URL: str = "sqlite+aiosqlite:///./cookbook.db"

engine: AsyncEngine = create_async_engine(DATABASE_URL, echo=True)
async_session: sessionmaker = sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)
session: Session = async_session()
Base: typing.Any = declarative_base()
