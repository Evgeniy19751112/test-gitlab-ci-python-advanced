from typing import List

from sqlalchemy.exc import DatabaseError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

import src.models as models
import src.schemas as schemas


async def create_dish(
    db: AsyncSession, dish: schemas.CookbookIn
) -> models.CookbookDishes:
    """Добавить в БД рецепт блюда"""
    new_item = models.CookbookDishes(**dish.dict())
    try:
        db.add(new_item)
        await db.commit()
    except DatabaseError:
        await db.rollback()
    finally:
        await db.refresh(new_item)
    return new_item


async def get_dishes_list(db: AsyncSession) -> List[models.CookbookDishes]:
    """Получить список рецептов из БД"""
    result = await db.execute(
        select(models.CookbookDishes).order_by(
            models.CookbookDishes.number_of_views.desc(),
            models.CookbookDishes.cooking_time,
        )
    )
    return result.scalars().all()


async def get_dish_by_id(db: AsyncSession, dish_id: int) -> models.CookbookDishes:
    """Получить список блюд по id из БД"""
    result = await db.execute(
        select(models.CookbookDishes).where(models.CookbookDishes.id == dish_id)
    )
    return result.scalars().one_or_none()


async def update_dish(
    db: AsyncSession, dish: models.CookbookDishes
) -> models.CookbookDishes:
    """Обновить запись в БД (есть сомнения в правильности такого подхода)"""
    try:
        await db.commit()
    except DatabaseError:
        await db.rollback()
    finally:
        await db.refresh(dish)
    return dish
