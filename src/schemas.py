""" Схемы для приложения """

from typing import Optional

from pydantic import BaseModel, Field


class BaseCookbook(BaseModel):
    dish_name: str = Field(
        title="Наименование блюда",
        description="Допускаются только уникальные названия блюд в базе данных",
    )
    cooking_time: float = Field(
        title="Время приготовления блюда",
        description="Время приготовления блюда в минутах. Дробная часть в "
        "десятичных долях от минуты",
        gt=0,
        lt=24 * 60,
    )


class CookbookIn(BaseCookbook):
    description: Optional[str] = Field(
        title="Подробное описание блюда",
        description="Весь процесс приготовления записывается тутачки.",
    )
    ingredients: Optional[str] = Field(
        title="Список продуктов для приготовления",
        description="Продукты перечисляются так, как будет удобно конечному "
        "пользователю. Также можно указать агрегатное состояние "
        "и пропорции.",
    )

    class Config:
        orm_mode = True


class CookbookOut(BaseCookbook):
    id: int
    number_of_views: Optional[int] = Field(
        title="Количество просмотров",
        description="Счётчик увеличивается при каждом подробном показе блюда.",
    )

    class Config:
        orm_mode = True
        from_attributes = True


class CookbookDetails(CookbookOut, CookbookIn):
    ...

    class Config:
        orm_mode = True
        from_attributes = True
