"""
Основной модуль программы. Запуск "uvicorn --reload main:app --host=0.0.0.0"
"""

from typing import AsyncGenerator, List

import uvicorn
from fastapi import Body, Depends, FastAPI, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.exc import DatabaseError
from sqlalchemy.ext.asyncio import AsyncSession

import src.crud as crud
import src.models as models
import src.schemas as schemas
from src.database import async_session, engine, session

app = FastAPI(
    debug=True,
    title="FastAPI & Cookbook",
    description="The result of completing the training task for creating "
    'the "Cookbook" API. But it could have been part of a '
    "real project.",
    tracemalloc=True,
)


async def get_db() -> AsyncGenerator[AsyncSession, None]:
    # type: ignore[attr-defined]
    async with async_session() as _session:
        try:
            yield _session
        except DatabaseError:
            await _session.rollback()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get("/", include_in_schema=False)
async def root():
    """Стартовая страница просто информирует о документации к API"""
    return {"message": "Use /docs or /redoc to get documentation"}


@app.get("/favicon.ico", response_class=FileResponse, include_in_schema=False)
async def favicon():
    """Отдадим браузеру нашу иконку. Это не обязательно, но с ней лучше."""
    return FileResponse("../static/favicon.ico")


@app.post("/cookbook/dishes", response_model=schemas.CookbookOut, status_code=201)
async def post_cookbook_dishes(
    cookbook: schemas.CookbookIn = Body(
        ...,
        title="Данные для добавления блюда",
        description="Полная информация для добавления в базу данных. "
        "Текущая версия не предусматривает редактирование "
        "описания блюд, но можно править через прямой "
        "доступ к базе данных.",
    ),
    db: AsyncSession = Depends(get_db),
) -> models.CookbookDishes:
    return await crud.create_dish(db=db, dish=cookbook)


@app.get("/cookbook/dishes", response_model=List[schemas.CookbookOut])
async def get_cookbook_dishes(
    db: AsyncSession = Depends(get_db),
) -> List[models.CookbookDishes]:
    return await crud.get_dishes_list(db=db)


@app.get("/cookbook/dishes/{id}", response_model=schemas.CookbookDetails)
async def get_cookbook_dishes_details(
    id: int, db: AsyncSession = Depends(get_db)
) -> models.CookbookDishes:
    dish = await crud.get_dish_by_id(db=db, dish_id=id)
    if dish:
        number_of_views: int = dish.number_of_views
        dish.number_of_views = number_of_views + 1
        return await crud.update_dish(db=db, dish=dish)
    raise HTTPException(404, "No such dish with id {}".format(id))


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
