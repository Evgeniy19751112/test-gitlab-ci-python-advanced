""" Описание моделей таблиц базы данных кулинарной книги """

from sqlalchemy import Column, DateTime, Float, Integer, String, func

from src.database import Base


class CookbookDishes(Base):
    """
    Название блюда - содержит название блюда, время приготовления,
    примечание (ссылки на картинки и/или описание процесса
    приготовления), перечень ингредиентов, счётчик просмотров.
    """

    __tablename__ = "dishes"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, default=func.now())
    dish_name = Column(String, unique=True, nullable=False)
    cooking_time = Column(Float, nullable=False)
    description = Column(String, nullable=True)
    ingredients = Column(String, nullable=True)
    number_of_views = Column(Integer, nullable=False, default=0)
